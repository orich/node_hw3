# Loads Service REST API
Loads Service allows your app or web page to implement loads application. It allows shippers to find apropriates cars for their loads.

# Install
1. Download zip archive from gitlab.
2. Unarchive zip file.
3. Run terminal app for entering comands.
4. Run install.

```bash
# install locally (recommended)
npm install
```

5. Run api server
```bash
nmp start inside node_hm3 folder
```

# Api server address
```bash
localhost:8080
```
For example full api url: http://localhost:8080/loads/

# Api end points

```bash
GET api/users/me
```
Gets user's profile info

```bash
GET api/users/me
```
Gets user's profile info
