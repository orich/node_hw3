const bcryptjs = require('bcryptjs');

const { UsersService } = require('../services/usersService');
const { processResult } = require('./controllerHelper');

class UsersController {
  static async getUserProfile(req, res, next) {
    const user = await UsersService.getUserProfile(req.user.userId);
    return processResult(
      user,
      { message: 'Can not get user profile' },
      { user },
      res,
    );
  }

  static async deleteUser(req, res, next) {
    const user = await UsersService.deleteUser(req.user.userId);
    return processResult(
      user,
      { message: 'Unable to delete user' },
      { message: 'Profile deleted successfully' },
      res,
    );
  }

  static async changeUserPassword(req, res, next) {
    const { oldPassword, newPassword } = req.body;
    const user = await UsersService.getUserById(req.user.userId);
    if (!user) {
      return res.status(403).json({ message: 'Not found' });
    }
    if (await bcryptjs.compare(String(oldPassword), String(user.password))) {
      const resault = await UsersService.updatePassword(req.user.userId, newPassword);
      if (!resault) {
        return res.status(400).json({ message: 'Unable to change password' });
      }
    } else {
      return res.status(400).json({ message: 'Old passport is wrong' });
    }

    return res.json({ message: 'Password changed successfully' });
  }
}

module.exports = {
  UsersController,
};
