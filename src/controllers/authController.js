const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const { processResult } = require('./controllerHelper');

dotenv.config();

const { UsersService: Service } = require('../services/usersService');

class AuthController {
  static async registerUser(req, res, next) {
    const { email, password, role } = req.body;

    await Service.saveUser({ email, password, role });
    return res.status(200).json({ message: 'Profile created successfully' });
  }

  static async loginUser(req, res, next) {
    const { email, password } = req.body;
    const user = await Service.getUser(email);
    if (user && await bcryptjs.compare(String(password), String(user.password))) {
      const payload = { email: user.email, userId: user._id, role: user.role };
      const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
      return res.json({ jwt_token: jwtToken });
    }
    return res.status(400).json({ message: 'Not authorized' });
  }

  static async forgotPassword(req, res, next) {
    const { email } = req.body;
    const user = await Service.getUser(email);
    if (!user) {
      return res.status(400).json({ message: 'Current user not registered' });
    }
    const resetPasswordToken = crypto.randomBytes(20).toString('hex');
    const resetPasswordExpires = Date.now() + 3600000;
    await Service.updateUserResetInfo(
      user._id,
      resetPasswordToken,
      resetPasswordExpires,
    );

    const link = `http://${req.headers.host}/api/auth/reset/${resetPasswordToken}`;

    const transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
      from: '"Fred Foo" <foo@example.com>', // sender address
      to: email,
      subject: 'Password change request',
      text: `Hi ${user.username} \n 
        Please click on the following link ${link} to reset your password. \n\n 
        If you did not request this, please ignore this email and your password will remain unchanged.\n`,
    });

    console.log('Message sent: %s', info.messageId);
    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    return res.json({ message: 'New password sent to your email address' });
  }

  static async resetPassword(req, res, next) {
    const result = Service.updatePasswordByResetToken(req.params.token, req.body.password);
    return processResult(
      result,
      { message: 'New password was not updated' },
      { message: 'Password was sucessfully updated' },
      res,
    );
  }
}

module.exports = {
  AuthController,
};
