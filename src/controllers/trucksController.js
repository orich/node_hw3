const { TrucksService: Service } = require('../services/trucksService');
const { processResult } = require('./controllerHelper');

class TrucksController {
  static async createTruck(req, res, next) {
    const truck = await Service.saveTruck(req.user.userId, req.body.type);
    return processResult(
      truck,
      { message: 'Truck was not created' },
      { message: 'Truck created successfully' },
      res,
    );
  }

  static async getTrucks(req, res, next) {
    const trucks = await Service.getTrucks(req.user.userId, +req.query.offset, +req.query.limit);
    return processResult(
      trucks,
      { message: 'No trucks for current driver' },
      { trucks },
      res,
    );
  }

  static async getTruckById(req, res, next) {
    const truck = await Service.getTruck(req.params.id, req.user.userId);
    return processResult(
      truck,
      { message: 'Truck not found' },
      { truck },
      res,
    );
  }

  static async updateTruck(req, res, next) {
    const truck = await Service.updateTruck(req.params.id, req.user.id, req.body.type);
    return processResult(
      truck,
      { message: 'Truck was not changed' },
      { message: 'Truck details changed successfully' },
      res,
    );
  }

  static async deleteTruck(req, res, next) {
    const truck = await Service.deleteTruck(req.params.id, req.user.id);
    return processResult(
      truck,
      { message: 'Truck was not deleted' },
      { message: 'Truck deleted successfully' },
      res,
    );
  }

  static async assignedTruck(req, res, next) {
    const truck = await Service.assignTrack(req.params.id, req.user.userId);
    return processResult(
      truck,
      { message: 'Truck was not assigned' },
      { message: 'Truck assigned successfull' },
      res,
    );
  }
}

module.exports = {
  TrucksController,
};
