const processResult = (result, failedRes, successRes, res) => {
  if (!result) {
    return res.status(400).json(failedRes);
  }
  return res.status(200).json(successRes);
};

module.exports = {
  processResult,
};
