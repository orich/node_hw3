const { LOAD_STATUS, LOAD_STATE } = require('../models/loads');
const { LoadsService: Service } = require('../services/loadsService');
const { processResult } = require('./controllerHelper');

class LoadsController {
  static async createLoad(req, res, next) {
    const load = req.body;
    load.created_by = req.user.userId;
    load.state = '';
    const result = await Service.saveLoad(load);
    return processResult(
      result,
      { message: 'Load was not created' },
      { message: 'Load created successfully' },
      res,
    );
  }

  static async updateLoad(req, res, next) {
    const load = req.body;
    const result = await Service.updateLoad(req.params.id, load, req.user.userId);
    return processResult(
      result,
      { message: 'Load details were not updated.' },
      { message: 'Load details changed successfully' },
      res,
    );
  }

  static async deleteLoad(req, res, next) {
    const result = await Service.deleteLoad(req.params.id, req.user.userId);
    return processResult(
      result.deletedCount,
      { message: 'Load was not deleted' },
      { message: 'Load deleted successfully' },
      res,
    );
  }

  static async getLoads(req, res, next) {
    const loads = await Service.getLoads(
      req.user.userId,
      req.user.role,
      req.query.status,
      +req.query.offset,
      +req.query.limit,
    );
    return processResult(
      loads,
      { message: 'No loads for current driver' },
      { loads },
      res,
    );
  }

  static async postLoad(req, res, next) {
    const { id: loadId } = req.params;
    const { userId } = req.user;
    const load = await Service.updateLoadStatus(loadId, LOAD_STATUS.posted);
    if (!load) {
      return res.status(400).json({ message: 'Load status did not changed to posted' });
    }
    const truck = await Service.findFreeTruck(loadId, userId);
    if (!truck.length) {
      await Service.updateLoadStatus(loadId, LOAD_STATUS.new);
      return res.status(400).json({ message: 'Could not find available truck for a load' });
    }
    const result = await Service.assignTruck(truck[0]._id, loadId);
    return processResult(
      result,
      { message: 'Failed to assign driver' },
      {
        message: 'Load posted successfully',
        driver_found: true,
      },
      res,
    );
  }

  static async getDriverActiveLoad(req, res, next) {
    const load = await Service.getUserActiveLoad(req.user.userId);
    return processResult(
      load,
      { message: 'User do not have active load' },
      {
        load,
      },
      res,
    );
  }

  static async getLoad(req, res, next) {
    const load = await Service.getLoad(req.params.id);
    return processResult(
      load,
      { message: 'Can not get this load' },
      {
        load,
      },
      res,
    );
  }

  static async setNextStateForDriverActiveLoad(req, res, next) {
    const load = await Service.setNextStateForDriverActiveLoad(req.user.userId);
    if (load && load.state === LOAD_STATE.ArrivedToDelivery) {
      await Service.updateLoadStatus(load._id, LOAD_STATUS.shipped);
      await Service.changeStatusForDriverActiveTruck(req.user.userId, 'IS');
    }
    return processResult(
      load,
      { message: 'State was not changed' },
      { message: `Load state changed to '${load?.state}'` },
      res,
    );
  }

  static async getLoadShippindInfo(req, res, next) {
    const info = await Service.getLoadShippindInfo(req.params.id, req.user.userId);
    let result;
    if (info.length) {
      [result] = info;
    }
    return processResult(
      info.length,
      { message: 'Unable to get shipping info' },
      { load: result?.load, truck: result?.truck },
      res,
    );
  }
}

module.exports = {
  LoadsController,
};
