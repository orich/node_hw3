const Joi = require('joi');

class BaseValidation {
  static get checkOffsetLimitParamsSchema() {
    return Joi.object({
      offset: Joi.number()
        .integer(),
      limit: Joi.number()
        .integer(),
    });
  }

  static get checkIdSchema() {
    return Joi.object({
      id: Joi.string()
        .hex().length(24)
        .required(),
    });
  }

  static get checkEmailShema() {
    return Joi.object({
      email: Joi.string()
        .email()
        .required()
        .messages({
          'string.email': '"email" must be in valid format',
        }),
    });
  }
}

module.exports = {
  BaseValidation,
};
