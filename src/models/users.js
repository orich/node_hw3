const mongoose = require('mongoose');
const Joi = require('joi');

const { BaseValidation } = require('./baseValidation');

const USER_ROLE = {
  driver: 'DRIVER',
  shipper: 'SHIPPER',
};

const User = mongoose.model('User', {
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
    required: true,
  },
  email: {
    type: String,
    trim: true,
    match: [/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/],
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
    default: new Date().toISOString(),
  },
  resetPasswordToken: {
    type: String,
    required: false,
  },
  resetPasswordExpires: {
    type: Date,
    required: false,
  },
});

class UsersValidation extends BaseValidation {
  static get registerUserSchema() {
    return Joi.object({
      email: Joi.string()
        .email()
        .required()
        .messages({
          'string.email': '"email" must be in valid format',
        }),

      password: Joi.string()
        .pattern(/[a-zA-Z0-9]{3,30}$/)
        .required(),

      role: Joi.string()
        .valid('DRIVER', 'SHIPPER')
        .required(),
    });
  }

  static get loginUserShema() {
    return Joi.object({
      email: Joi.string()
        .email()
        .required()
        .messages({
          'string.email': '"email" must be in valid format',
        }),

      password: Joi.string()
        .required(),
    });
  }

  static get changePasswordSchema() {
    return Joi.object({
      oldPassword: Joi.string()
        .pattern(/[a-zA-Z0-9]{3,30}$/)
        .required(),

      newPassword: Joi.string()
        .pattern(/[a-zA-Z0-9]{3,30}$/)
        .required(),
    });
  }

  static get resetPasswordShema() {
    return Joi.object({
      password: Joi.string()
        .min(3).max(15).required(),
      passwordConfirmation: Joi.any()
        .valid(Joi.ref('password'))
        .required(),
    });
  }
}

module.exports = {
  User,
  UsersValidation,
  USER_ROLE,
};
