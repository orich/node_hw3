const mongoose = require('mongoose');
const Joi = require('joi');

const { BaseValidation } = require('./baseValidation');

const TRUCK_TYPES = {
  SPRINTER: {
    payload: 1700,
    length: 300,
    width: 250,
    height: 170,
  },
  'SMALL STRAIGHT': {
    payload: 2500,
    length: 500,
    width: 250,
    height: 170,
  },
  'LARGE STRAIGHT': {
    payload: 4000,
    length: 700,
    width: 350,
    height: 200,
  },
};

const Truck = mongoose.model('Truck', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    required: true,
    default: 'IS',
  },
  created_date: {
    type: String,
    required: true,
    default: new Date().toISOString(),
  },
});

class TrucksValidation extends BaseValidation {
  static get createTruckSchema() {
    return Joi.object({
      type: Joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required(),
    });
  }
}

module.exports = {
  TRUCK_TYPES,
  Truck,
  TrucksValidation,
};
