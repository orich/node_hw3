const mongoose = require('mongoose');
const Joi = require('joi');

const { BaseValidation } = require('./baseValidation');

const LOAD_STATUS = {
  new: 'NEW',
  posted: 'POSTED',
  assigned: 'ASSIGNED',
  shipped: 'SHIPPED',
};

const LOAD_STATE = {
  EnRouteToPickUp: 'En route to Pick Up',
  ArrivedToPickUp: 'Arrived to Pick Up',
  EnRouteToDelivery: 'En route to delivery',
  ArrivedToDelivery: 'Arrived to delivery',
};

const Load = mongoose.model('Load', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    required: true,
    default: 'NEW',
  },
  state: {
    type: String,
    enum: ['', 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
    default: '',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [{
    _id: false,
    message: String,
    time: {
      type: Date,
      default: new Date().toISOString(),
    },
  }],
  created_date: {
    type: String,
    required: true,
    default: new Date().toISOString(),
  },
});

class LoadsValidation extends BaseValidation {
  static get createLoadSchema() {
    return Joi.object({
      name: Joi.string()
        .required(),
      payload: Joi.number()
        .required(),
      pickup_address: Joi.string()
        .required(),
      delivery_address: Joi.string()
        .required(),
      dimensions: Joi.object({
        width: Joi.number()
          .required(),
        length: Joi.number()
          .required(),
        height: Joi.number()
          .required(),
      }),
    });
  }

  static get checkGetLoadsSchema() {
    return Joi.object({
      status: Joi.string()
        .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'),
      offset: Joi.number()
        .integer(),
      limit: Joi.number()
        .integer(),
    });
  }
}

module.exports = {
  Load,
  LoadsValidation,
  LOAD_STATE,
  LOAD_STATUS,
};
