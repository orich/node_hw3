const express = require('express');
const { asyncWrapper } = require('./routerHelper');

const router = express.Router();
const { UsersController } = require('../controllers/usersController');
const { UsersMiddleware } = require('../middleware/usersMiddleware');
const { UsersValidation: Validator } = require('../models/users');
const { validateRequest, VALIDATION_TYPE } = require('../middleware/validateRequestMiddleware');

router.get(
  '/me',
  asyncWrapper(UsersController.getUserProfile),
);

router.patch(
  '/me/password',
  validateRequest(Validator.changePasswordSchema, VALIDATION_TYPE.body),
  asyncWrapper(UsersController.changeUserPassword),
);

router.delete(
  '/me',
  UsersMiddleware.isUserAllowedUpdateData,
  asyncWrapper(UsersController.deleteUser),
);

module.exports = {
  usersRouter: router,
};
