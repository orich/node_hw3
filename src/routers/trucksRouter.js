const express = require('express');
const { asyncWrapper } = require('./routerHelper');
const { TrucksValidation: Validator } = require('../models/trucks');
const { UsersMiddleware } = require('../middleware/usersMiddleware');
const { TrucksMiddleware } = require('../middleware/trucksMiddleware');

const router = express.Router();
const { TrucksController: Controller } = require('../controllers/trucksController');
const { validateRequest, VALIDATION_TYPE } = require('../middleware/validateRequestMiddleware');

router.post(
  '/',
  validateRequest(Validator.createTruckSchema, VALIDATION_TYPE.body),
  asyncWrapper(Controller.createTruck),
);

router.get(
  '/',
  validateRequest(Validator.checkOffsetLimitParamsSchema, VALIDATION_TYPE.query),
  asyncWrapper(Controller.getTrucks),
);

router.get(
  '/:id',
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  asyncWrapper(Controller.getTruckById),
);

router.put(
  '/:id',
  UsersMiddleware.isUserAllowedUpdateData,
  TrucksMiddleware.IsTruckNotAssignedToCurrentUser,
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  validateRequest(Validator.createTruckSchema, VALIDATION_TYPE.body),
  asyncWrapper(Controller.updateTruck),
);

router.delete(
  '/:id',
  UsersMiddleware.isUserAllowedUpdateData,
  TrucksMiddleware.IsTruckNotAssignedToCurrentUser,
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  asyncWrapper(Controller.deleteTruck),
);

router.post(
  '/:id/assign',
  UsersMiddleware.isUserAllowedUpdateData,
  TrucksMiddleware.IsTruckNotAssignedToCurrentUser,
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  asyncWrapper(Controller.assignedTruck),
);

module.exports = {
  trucksRouter: router,
};
