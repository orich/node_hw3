const express = require('express');
const { asyncWrapper } = require('./routerHelper');

const router = express.Router();
const { AuthController: Controller } = require('../controllers/authController');
const { UsersValidation: Validator } = require('../models/users');
const { validateRequest, VALIDATION_TYPE } = require('../middleware/validateRequestMiddleware');
const { AuthMiddleware } = require('../middleware/authMiddleware');

router.post(
  '/register',
  validateRequest(Validator.registerUserSchema, VALIDATION_TYPE.body),
  asyncWrapper(Controller.registerUser),
);

router.post(
  '/login',
  validateRequest(Validator.loginUserShema, VALIDATION_TYPE.body),
  asyncWrapper(Controller.loginUser),
);

router.post(
  '/forgot_password',
  validateRequest(Validator.checkEmailShema, VALIDATION_TYPE.body),
  asyncWrapper(Controller.forgotPassword),
);

router.post(
  '/reset/:token',
  AuthMiddleware.verifyResetToken,
  validateRequest(Validator.resetPasswordShema, VALIDATION_TYPE.body),
  asyncWrapper(Controller.resetPassword),
);

module.exports = {
  authRouter: router,
};
