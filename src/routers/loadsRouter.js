const express = require('express');
const { asyncWrapper } = require('./routerHelper');

const { LoadsValidation: Validator } = require('../models/loads');
const { UsersMiddleware } = require('../middleware/usersMiddleware');
const { LoadsMiddleware } = require('../middleware/loadsMiddleware');

const router = express.Router();
const { LoadsController: Controller } = require('../controllers/loadsController');
const { validateRequest, VALIDATION_TYPE } = require('../middleware/validateRequestMiddleware');

router.post(
  '/',
  UsersMiddleware.isUserShipper,
  validateRequest(Validator.createLoadSchema, VALIDATION_TYPE.body),
  asyncWrapper(Controller.createLoad),
);

router.post(
  '/:id',
  UsersMiddleware.isUserShipper,
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  validateRequest(Validator.createLoadSchema, VALIDATION_TYPE.body),
  asyncWrapper(Controller.updateLoad),
);

router.delete(
  '/:id',
  UsersMiddleware.isUserShipper,
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  asyncWrapper(Controller.deleteLoad),
);

router.get(
  '/:id/shipping_info',
  UsersMiddleware.isUserShipper,
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  asyncWrapper(Controller.getLoadShippindInfo),
);

router.get(
  '/',
  LoadsMiddleware.makeUpperCaseStatus,
  validateRequest(Validator.checkGetLoadsSchema, VALIDATION_TYPE.query),
  asyncWrapper(Controller.getLoads),
);

router.post(
  '/:id/post',
  UsersMiddleware.isUserShipper,
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  asyncWrapper(Controller.postLoad),
);

router.get(
  '/active',
  UsersMiddleware.isUserDriver,
  asyncWrapper(Controller.getDriverActiveLoad),
);

router.patch(
  '/active/state',
  UsersMiddleware.isUserDriver,
  asyncWrapper(Controller.setNextStateForDriverActiveLoad),
);

router.get(
  '/:id',
  validateRequest(Validator.checkIdSchema, VALIDATION_TYPE.params),
  asyncWrapper(Controller.getLoad),
);

module.exports = {
  loadsRouter: router,
};
