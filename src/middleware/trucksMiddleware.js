const { TrucksService: Service } = require('../services/trucksService');

class TrucksMiddleware {
  static makeUpperCaseStatus(req, res, next) {
    if (req.query.status) {
      req.query.status = req.query.status.toUpperCase();
    }
    return next();
  }

  static async IsTruckNotAssignedToCurrentUser(req, res, next) {
    const result = await Service.IsTruckAssignedToCurrentUser(req.params.id, req.user.userId);
    if (result) {
      return res.status(400).json({ message: "Driver can't modify assigned to him truck" });
    }
    return next();
  }
}

module.exports = {
  TrucksMiddleware,
};
