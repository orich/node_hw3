class LoadsMiddleware {
  static makeUpperCaseStatus(req, res, next) {
    if (req.query.status) {
      req.query.status = req.query.status.toUpperCase();
    }
    return next();
  }
}

module.exports = {
  LoadsMiddleware,
};
