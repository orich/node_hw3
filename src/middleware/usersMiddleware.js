const { UsersService: Service } = require('../services/usersService');
const { USER_ROLE } = require('../models/users');

class UsersMiddleware {
  static isUserDriver(req, res, next) {
    if (req.user.role !== USER_ROLE.driver) {
      return res.status(400).json({ message: "Shipper can't make this changes" });
    }
    return next();
  }

  static isUserShipper(req, res, next) {
    if (req.user.role !== USER_ROLE.shipper) {
      return res.status(400).json({ message: "Driver can't make this changes" });
    }
    return next();
  }

  static async isUserAllowedUpdateData(req, res, next) {
    if (await Service.isUserDriverAndOnLoad(req.user.userId)) {
      return res.status(400).json({ message: "Driver can't made changes since he is on a load" });
    }
    return next();
  }
}

module.exports = {
  UsersMiddleware,
};
