const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const { UsersService: Service } = require('../services/usersService');

dotenv.config();

class AuthMiddleware {
  static async verifyAuthToken(req, res, next) {
    const {
      authorization,
    } = req.headers;

    if (!authorization) {
      return res.status(401).json({ message: 'Please, provide authorization header' });
    }

    const [, token] = authorization.split(' ');

    if (!token) {
      return res.status(401).json({ message: 'Please, include token to request' });
    }

    try {
      const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
      req.user = {
        userId: tokenPayload.userId,
        email: tokenPayload.email,
        role: tokenPayload.role,
      };
      return next();
    } catch (err) {
      return res.status(401).json({ message: err.message });
    }
  }

  static async verifyResetToken(req, res, next) {
    try {
      const user = await Service.isResetTockenValid(req.params.token);
      if (!user) {
        return res.status(401).json({ message: 'Password reset token is invalid or has expired.' });
      }
      return next();
    } catch (err) {
      return next(err);
    }
  }
}

module.exports = {
  AuthMiddleware,
};
