const validateRequest = (joiSchema, requestObjName) => async (req, res, next) => {
  try {
    await joiSchema.validateAsync(req[requestObjName]);
  } catch (err) {
    return next(err);
  }
  return next();
};

const VALIDATION_TYPE = {
  query: 'query',
  params: 'params',
  body: 'body',
};

module.exports = {
  validateRequest,
  VALIDATION_TYPE,
};
