const { Truck } = require('../models/trucks');

class TrucksService {
  static async saveTruck(userId, type) {
    const user = new Truck({
      created_by: userId,
      type,
    });

    return user.save();
  }

  static async getTrucks(userId, offset, limit) {
    return Truck.find({ created_by: userId })
      .select('-__v')
      .skip(offset)
      .limit(limit);
  }

  static async getTruck(truckId, userId) {
    return Truck.findOne({ _id: truckId, created_by: userId }).select('-__v');
  }

  static async updateTruck(truckId, userId, type) {
    const truck = await Truck.findOne({ _id: truckId, userId });
    if (!truck) return null;
    return Truck
      .findByIdAndUpdate({ _id: truckId, created_by: userId }, { $set: { type } });
  }

  static async deleteTruck(truckId, userId) {
    return Truck.findOneAndDelete({ _id: truckId, userId });
  }

  static async assignTrack(truckId, userId) {
    // unassign old track
    await Truck.findOneAndUpdate(
      { assigned_to: userId, created_by: userId },
      { $set: { assigned_to: null } },
    );
    // assigned curren truck
    return Truck.findOneAndUpdate(
      { _id: truckId, created_by: userId },
      { $set: { assigned_to: userId } },
    );
  }

  static async IsTruckAssignedToCurrentUser(truckId, userId) {
    return Truck.findOne({ _id: truckId, assigned_to: userId });
  }
}

module.exports = {
  TrucksService,
};
