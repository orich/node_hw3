const { Load, LOAD_STATUS, LOAD_STATE } = require('../models/loads');
const { Truck } = require('../models/trucks');
const { USER_ROLE } = require('../models/users');

const { getLoadShippindInfoPipline } = require('./agregations/getLoadShippingInfo');
const { findFreeTruckPipline } = require('./agregations/findFreeTruck');

const getLogObject = (message) => ({
  $push: {
    logs: {
      message,
    },
  },
});

class LoadsService {
  static async saveLoad(data) {
    const load = new Load(data);

    return load.save();
  }

  static async updateLoad(loadId, data, userId) {
    return Load.findOneAndUpdate(
      { _id: loadId, created_by: userId, status: LOAD_STATUS.new },
      {
        ...data,
        ...getLogObject('Load was updated'),
      },
      { new: true },
    );
  }

  static async getLoads(userId, role, loadStatus, offset, limit) {
    let filter = { created_by: userId };
    if (role === USER_ROLE.driver) {
      filter = { assigned_to: userId };
    }
    if (loadStatus) {
      filter.status = loadStatus;
    }
    return Load.find(filter)
      .select('-__v')
      .skip(offset)
      .limit(limit);
  }

  static async updateLoadStatus(loadId, status) {
    let logObj;
    switch (status) {
      case LOAD_STATUS.new:
        logObj = getLogObject('Driver was not found. Load was set back to NEW status.');
        break;
      case LOAD_STATUS.posted:
        logObj = getLogObject('Load was posted');
        break;
      case LOAD_STATUS.shipped:
        logObj = getLogObject('Load was shipped');
        break;
      default:
        logObj = {};
    }
    return Load.findByIdAndUpdate(
      loadId,
      { status, ...logObj },
      { new: true },
    );
  }

  static async findFreeTruck(loadId, userId) {
    const load = await Load.findOne({ _id: loadId, created_by: userId });
    const freeTruck = await Truck.aggregate(findFreeTruckPipline(load));
    return freeTruck;
  }

  static async assignTruck(truckId, loadId) {
    const truck = await Truck.findByIdAndUpdate(truckId, { status: 'OL' });
    const load = await Load.findByIdAndUpdate(loadId, {
      state: LOAD_STATE.EnRouteToPickUp,
      status: LOAD_STATUS.assigned,
      assigned_to: truck.assigned_to,
      $push: {
        logs: {
          $each: [
            {
              message: `Load assigned to driver with id '${truck.assigned_to}'`,
            },
            {
              message: `Load was set to state '${LOAD_STATE.EnRouteToPickUp}'`,
            },
          ],
        },
      },
    });
    return { load, truck };
  }

  static async getUserActiveLoad(userId) {
    return Load.findOne({ assigned_to: userId, status: LOAD_STATUS.assigned });
  }

  static async getLoad(loadId) {
    return Load.findById(loadId).select('-__v');
  }

  static async setNextStateForDriverActiveLoad(userId) {
    const load = await Load.findOne({ assigned_to: userId, status: LOAD_STATUS.assigned });
    if (!load || !load.state || load.state === LOAD_STATE.ArrivedToDelivery) {
      return null;
    }
    const states = Object.values(LOAD_STATE);
    const index = states.indexOf(load.state) + 1;
    const nextState = states[index];
    return Load.findOneAndUpdate(
      { assigned_to: userId, status: LOAD_STATUS.assigned },
      {
        state: nextState,
        ...getLogObject(`Driver changed load state to '${nextState}'`),
      },
      { new: true },
    );
  }

  static async changeStatusForDriverActiveTruck(userId, status) {
    return Truck.findOneAndUpdate({ assigned_to: userId }, { status }, { new: true });
  }

  static async deleteLoad(loadId, userId) {
    return Load.deleteOne({ _id: loadId, created_by: userId, status: LOAD_STATUS.new });
  }

  static async getLoadShippindInfo(loadId, userId) {
    return Load.aggregate(getLoadShippindInfoPipline(loadId, userId));
  }
}

module.exports = {
  LoadsService,
};
