const bcryptjs = require('bcryptjs');
const { User } = require('../models/users');
const { Truck } = require('../models/trucks');

class UsersService {
  static async getUser(email) {
    return User.findOne({ email });
  }

  static async getUserById(userId) {
    return User.findById(userId);
  }

  static async saveUser({ email, password, role }) {
    const user = new User({
      email,
      password: await bcryptjs.hash(password, 10),
      role,
    });

    return user.save();
  }

  static async getUserProfile(userId) {
    return User.findOne({ _id: userId }).select('-__v -password');
  }

  static async deleteUser(userId) {
    return User.findByIdAndDelete(userId);
  }

  static async updatePassword(userId, password) {
    return User
      .findByIdAndUpdate(userId, { $set: { password: await bcryptjs.hash(password, 10) } });
  }

  static async updatePasswordByResetToken(token, password) {
    return User.findOneAndUpdate(
      { resetPasswordToken: token },
      { password: await bcryptjs.hash(password, 10) },
    );
  }

  static async isUserDriverAndOnLoad(userId) {
    const truck = await Truck.findOne({ created_by: userId, role: 'DRIVER', status: 'OL' });
    if (!truck) {
      return false;
    }
    return true;
  }

  static async updateUserResetInfo(userId, resetPasswordToken, resetPasswordExpires) {
    return User.findByIdAndUpdate(userId, { resetPasswordToken, resetPasswordExpires });
  }

  static async isResetTockenValid(token) {
    return User.findOne({ resetPasswordToken: token, resetPasswordExpires: { $gt: Date.now() } });
  }
}

module.exports = {
  UsersService,
};
