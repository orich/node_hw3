const mongoose = require('mongoose');

const getLoadShippindInfoPipline = (loadId, userId) => [
  {
    $match: {
      status: 'ASSIGNED',
      _id: new mongoose.Types.ObjectId(loadId),
      created_by: new mongoose.Types.ObjectId(userId),
    },
  },
  {
    $replaceRoot: {
      newRoot: {
        load: '$$ROOT',
      },
    },
  },
  {
    $lookup: {
      from: 'trucks',
      localField: 'load.assigned_to',
      foreignField: 'assigned_to',
      as: 'truck',
    },
  },
  {
    $unwind: '$truck',
  },
  {
    $project: {
      load: {
        __v: 0,
      },
      truck: {
        __v: 0,
      },
    },
  },
  { $limit: 1 },
];

module.exports = {
  getLoadShippindInfoPipline,
};
