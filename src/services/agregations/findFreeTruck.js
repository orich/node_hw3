const { TRUCK_TYPES } = require('../../models/trucks');

const findFreeTruckPipline = (load) => [
  {
    $match: {
      $expr: {
        $and: [
          {
            $eq: ['$status', 'IS'],
          },
          {
            $ne: ['$assigned_to', null],
          },
          {
            $switch: {
              branches: [
                {
                  case: { $eq: ['$type', 'SPRINTER'] },
                  then: {
                    $and: [
                      { $gt: [TRUCK_TYPES.SPRINTER.payload, load.payload] },
                      { $gt: [TRUCK_TYPES.SPRINTER.width, load.dimensions.width] },
                      { $gt: [TRUCK_TYPES.SPRINTER.height, load.dimensions.height] },
                      { $gt: [TRUCK_TYPES.SPRINTER.length, load.dimensions.length] },
                    ],
                  },
                },
                {
                  case: { $eq: ['$type', 'SMALL STRAIGHT'] },
                  then: {
                    $and: [
                      { $gt: [TRUCK_TYPES['SMALL STRAIGHT'].payload, load.payload] },
                      { $gt: [TRUCK_TYPES['SMALL STRAIGHT'].width, load.dimensions.width] },
                      { $gt: [TRUCK_TYPES['SMALL STRAIGHT'].height, load.dimensions.height] },
                      { $gt: [TRUCK_TYPES['SMALL STRAIGHT'].length, load.dimensions.length] },
                    ],
                  },
                },
                {
                  case: { $eq: ['$type', 'LARGE STRAIGHT'] },
                  then: {
                    $and: [
                      { $gt: [TRUCK_TYPES['LARGE STRAIGHT'].payload, load.payload] },
                      { $gt: [TRUCK_TYPES['LARGE STRAIGHT'].width, load.dimensions.width] },
                      { $gt: [TRUCK_TYPES['LARGE STRAIGHT'].height, load.dimensions.height] },
                      { $gt: [TRUCK_TYPES['LARGE STRAIGHT'].length, load.dimensions.length] },
                    ],
                  },
                },
              ],
              default: false,
            },
          },
        ],
      },
    },
  },
  { $limit: 1 },
];

module.exports = {
  findFreeTruckPipline,
};
